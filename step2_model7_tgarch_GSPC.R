#####################################################################
## FM442: Summative Presentation
## Step 2, Model 6: TGARCH with student t
## Authors: Evan Chow & Daniel Kullolli
##
## Model description: this is an TGARCH model:
## https://github.com/cran/rugarch/blob/master/inst/rugarch.tests/rugarch.test3.R
## Focuses on close price.
#####################################################################

MODEL.NAME <- "TGARCH"
OUTPUT.DIR <- glue("step2_model7_TGARCH")

#####################################################################
### .rprofile imports and setup
set.seed(42)
suppressMessages(lapply(c("ggplot2", "dplyr", "lubridate", "ggthemes",
                          "glue", "data.table", "chron", "rugarch"),
                        require, character.only=T))
options(scipen=999) # disable scientific notation
hheader <- function(x) { print("###############"); print(glue("# {x}")); print("###############"); }
pprint <- function(x, newline=2) { cat(x); for (i in 1:newline) { cat("\n") } }
YYYYMMDD <- "%Y%m%d"; YYYYMMDD.DASH <- "%Y-%m%d"
FIG.WIDTH <- 13; FIG.HEIGHT <- 8

INPUT.DIR <- glue("step1_data2")
if (!dir.exists(OUTPUT.DIR)) { dir.create(OUTPUT.DIR, showWarnings = FALSE) }
prefix.output <- function(x) glue("{OUTPUT.DIR}/{x}")

# constants (should move to JSON)
START.YR <- 2010; END.YR <- 2020
START.DS <- ymd(glue("{START.YR}0101")); END.DS <- ymd(glue("{END.YR}1101"))

#####################################################################
### Read in cleaned data

fp.GSPC.cleaned <- glue("{INPUT.DIR}/GSPC_cleaned__{format(START.DS, YYYYMMDD)}_{format(END.DS, YYYYMMDD)}.csv")
if (!exists("df.GSPC")) {
  colClasses <- c("Date", "numeric", "numeric", "numeric", "numeric", "logical", "logical", "logical")
  df.GSPC <- data.frame(fread(fp.GSPC.cleaned, colClasses = colClasses))
  df.GSPC$Date <- ymd(df.GSPC$Date)
}
num.cols <- colnames(df.GSPC)[sapply(df.GSPC, is.numeric)] # open, high, low, close

# for ggplot2: quarterly breaks
breaks_qtr = seq(from = min(df.GSPC$Date), to = max(df.GSPC$Date), by = "3 months")
labels_year = format(seq(from = min(df.GSPC$Date), to = max(df.GSPC$Date), by = "1 year"), "%Y")
labs = c(sapply(labels_year, function(x) { c(x, rep("", 3)) }))

# equivalent to is_trading_day == 1
df.GSPC.trading_days <- df.GSPC[complete.cases(df.GSPC),]

#####################################################################
#####################################################################
#####################################################################
### Calculate GARCH(p,q) models
#####################################################################
#####################################################################
#####################################################################

require(tseries)

### Use log returns
### all that stuff from lecture. 
### - if don't make stationary then will blow up over time
GSPC.returns <- df.GSPC.trading_days$Close
GSPC.returns <- GSPC.returns / dplyr::lag(GSPC.returns, 1)
GSPC.returns <- log(GSPC.returns)
qplot(df.GSPC.trading_days$Date, GSPC.returns) + geom_line() + 
  xlab("") + ylab("Log returns") + theme_economist_white()
# centered around 0 which is good - kinda stationary
acf(GSPC.returns[!is.na(GSPC.returns)])
pacf(GSPC.returns[!is.na(GSPC.returns)])
# some autocorrelation which is good (some volatilty clustering)

# only look at date #2 onward
GSPC.returns <- GSPC.returns[2:length(GSPC.returns)]
df.GSPC.trading_days <- df.GSPC.trading_days[2:nrow(df.GSPC.trading_days),]

#####################################################################
#####################################################################
#####################################################################
### Model and visualization - baseline example
#####################################################################
#####################################################################
#####################################################################

### Notes:
# note: log(x_{t+1}/x_t)*100 ~ (x_{t+1} / x_t - 1)*100
# For model annualised percentage:
# https://quant.stackexchange.com/questions/9190/garch1-1-prediction-in-r-basic-questions
# For VIX annualised percentage:
# https://www.cboe.com/publish/methodology-volatility/VIX_Methodology.pdf
# Note VIX is already annualised and in percentage terms, per the N_365/N_30 in the 
# formula and the coefficient 100.

# fit model
pq.order <- c(1,1)
garchspec <- ugarchspec(
  variance.model = list(model = "fGARCH", garchOrder = pq.order,submodel = "TGARCH"), 
  mean.model = list(armaOrder = c(1,1), include.mean = TRUE), 
  distribution.model = "std")
garch.pq <- ugarchfit(data=GSPC.returns, spec=garchspec)

# annualise model estimatations as percentage
fitted.garch.pq <- as.numeric(sigma(garch.pq)) ** 2 # sigma^2
fitted.garch.pq <- sqrt(fitted.garch.pq) * sqrt(250) * 100
df.fitted.garch.pq <- data.frame(Date=df.GSPC.trading_days$Date, Close=fitted.garch.pq)
model.name.pq <- glue("{MODEL.NAME}_{paste(c('p','q'),c(1,1), sep='',collapse='_')}")

# visualization
plt.garch.pq <- ggplot(df.fitted.garch.pq,
                       aes(x=Date, y=Close)) +
  geom_line() + theme_economist_white() + xlab("") + ylab("Close price") +
  ggtitle(glue("{model.name.pq} index, {START.DS} to {END.DS}")) +
  scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
  # scale_x_date(date_breaks = "1 year", date_minor_breaks = "3 month", date_labels = "%b-%y") + 
  theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14))
fp.viz <- prefix.output(glue("{model.name.pq}.png"))
ggsave(fp.viz, plt.garch.pq, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)
# save data too
fp.viz.data <- prefix.output(glue("{model.name.pq}_fitted.csv"))
fwrite(df.fitted.garch.pq, fp.viz.data)

# compare annualised GARCH with VIX (already annualised), in % terms
df.VIX <- fread("step2_model1_VIX/VIX_fitted.csv") %>% mutate(Date=ymd(Date), group="VIX")

avg.GSPC.return <- mean(df.GSPC.trading_days$Close) # could better approx, e.g. MA(30)
ex.garch.vol.annualised.pct <- df.fitted.garch.pq[,"Close"] # already converted to sigma^2
ex.VIX.sd.annualised.pct <- (df.VIX[df.VIX$Date %in% df.GSPC.trading_days$Date, "Close"])
df.ex <- data.frame(date=df.GSPC.trading_days$Date,
                    garch=ex.garch.vol.annualised.pct, VIX=ex.VIX.sd.annualised.pct)
df.ex.melt <- reshape2::melt(df.ex, id.vars=c("date"))
plt.ex.melt <- ggplot(df.ex.melt, aes(x=date, y=value, group=variable, color=variable)) +
  geom_line() + xlab("") + ylab("Annualised volatility (%)") + theme_economist_white()
show(plt.ex.melt)

#####################################################################
#####################################################################
#####################################################################
### Model and visualization - for all specifications
###
###
#####################################################################
#####################################################################
#####################################################################

all.garch.models <- list()
all.garch.data <- list()
for (p in 1:5) { # just set p, q = 1:1 if no parameters to tune
  for (q in 1:5) {
    hheader(glue("Running {MODEL.NAME}({p},{q})"))
    res <- tryCatch({
      # model
      pq.order <- c(p,q)
      garchspec <- ugarchspec(
        variance.model = list(model = "fGARCH", garchOrder = pq.order,submodel = "TGARCH"), 
        mean.model = list(armaOrder = c(1,1), include.mean = TRUE), 
        distribution.model = "std")
      garch.pq <- ugarchfit(data=GSPC.returns, spec=garchspec)
      
      # annualise model estimatations as percentage
      fitted.garch.pq <- as.numeric(sigma(garch.pq)) ** 2 # sigma^2
      fitted.garch.pq <- sqrt(fitted.garch.pq) * sqrt(250) * 100
      df.fitted.garch.pq <- data.frame(Date=df.GSPC.trading_days$Date, Close=fitted.garch.pq)
      model.name.pq <- glue("{MODEL.NAME}_{paste(c('p','q'), sep='',collapse='_')}")
      
      # # visualization
      # plt.garch.pq <- ggplot(df.fitted.garch.pq,
      #                        aes(x=Date, y=Close)) +
      #   geom_line() + theme_economist_white() + xlab("") + ylab("Close price") +
      #   ggtitle(glue("{model.name.pq} index, {START.DS} to {END.DS}")) +
      #   scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
      #   # scale_x_date(date_breaks = "1 year", date_minor_breaks = "3 month", date_labels = "%b-%y") + 
      #   theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14))
      # fp.viz <- prefix.output(glue("{model.name.pq}.png"))
      # ggsave(fp.viz, plt.garch.pq, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)
      # # save data too
      # fp.viz.data <- prefix.output(glue("{model.name.pq}_fitted.csv"))
      # fwrite(df.fitted.garch.pq, fp.viz.data)
      
      all.garch.models[[length(all.garch.models)+1]] <- list(
        model.name.pq=model.name.pq, p=p, q=q, model=garch.pq,
        AIC=infocriteria(garch.pq)['Akaike',], BIC=infocriteria(garch.pq)['Bayes',], data=df.fitted.garch.pq
      )
      all.garch.data[[length(all.garch.data)+1]] <- (
        df.fitted.garch.pq %>% mutate(group=glue("{MODEL.NAME}({p},{q})")))
      
      # ### save individual data too - only if need
      # fp.viz.data <- prefix.output(glue("{model.name.pq}_fitted.csv"))
      # fwrite(df.fitted.garch.pq, fp.viz.data)
    }, error=function(e) {
      print("Found error, continuing ...")
      print(e)
    }, finally = {
      print("continuing")
    })
  }
}
df.all.garch <- do.call(rbind, all.garch.data)
### overall, which have the best AIC and BIC (both)
### It seems AIC and BIC both return roughly the same best models (for GARCH)
### so I will just use AICs.
AICs <- unlist(sapply(all.garch.models, function(x) x['AIC']))
best.AICs.ixs <- sort(AICs, index.return=TRUE)$ix
# BICs <- unlist(sapply(all.garch.models, function(x) x['BIC']))
# best.BICs.ixs <- sort(AICs, index.return=TRUE)$ix
### combine both
best.AIC_and_BIC.ixs <- sort(AICs, index.return=TRUE)$ix[1]
best.garch.models <- all.garch.models[best.AIC_and_BIC.ixs]
best.garch.data <- all.garch.data[best.AIC_and_BIC.ixs]
df.best.all.garch <- do.call(rbind, best.garch.data) # selected three best based on AIC and BIC both
df.plt <- df.best.all.garch
# save data before appending VIX
fp.viz.data <- prefix.output(glue("{MODEL.NAME}_fitted.csv"))
fwrite(df.plt, fp.viz.data)

# plot VIX as well for comparison
df.VIX <- fread("step2_model1_VIX/VIX_fitted.csv") %>% mutate(Date=ymd(Date), group="VIX")
df.VIX <- df.VIX[df.VIX$Date %in% df.best.all.garch$Date,]
df.plt <- dplyr::bind_rows(df.plt, df.VIX)

# plot models and VIX
plt.all.garch <- ggplot(df.plt, aes(x=Date, y=Close, group=group, color=group)) +
  geom_line() + xlab("") + ylab("Annualized volatility (%)") +
  ggtitle(glue("{MODEL.NAME} for GSPC, {START.DS} to {END.DS}, with VIX for comparison")) +
  theme_economist_white() +
  scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
  theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14),
        legend.position="right", legend.title=element_blank())
fp.viz <- prefix.output(glue("{MODEL.NAME}.png"))
ggsave(fp.viz, plt.all.garch, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)




