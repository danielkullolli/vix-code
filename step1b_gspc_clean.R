#####################################################################
## FM442: Summative Presentation
## Step 1, Data 1: S&P-500 (vix underlying), data cleaning
## Authors: Evan Chow & Daniel Kullolli
##
## Input: GSPC.csv (S&P data)
#####################################################################

#####################################################################
### .rprofile imports and setup
set.seed(42)
suppressMessages(lapply(c("ggplot2", "dplyr", "lubridate", "ggthemes",
                          "glue", "data.table", "chron"),
                        require, character.only=T))
options(scipen=999) # disable scientific notation
hheader <- function(x) { print("###############"); print(glue("# x")); print("###############"); }
pprint <- function(x, newline=2) { cat(x); for (i in 1:newline) { cat("\n") } }
YYYYMMDD <- "%Y%m%d"; YYYYMMDD.DASH <- "%Y-%m%d"

OUTPUT.DIR <- glue("step1_data2")
if (!dir.exists(OUTPUT.DIR)) { dir.create(OUTPUT.DIR, showWarnings = FALSE) }

# constants (should move to JSON)
START.YR <- 2010; END.YR <- 2020
START.DS <- ymd(glue("{START.YR}0101")); END.DS <- ymd(glue("{END.YR}1101"))

#####################################################################
### Read in data and preprocess

if (!exists("df.VIX.raw")) {
  df.VIX.raw <- data.frame(fread("GSPC.csv"))
  df.VIX.raw$Volume <- NULL
  colnames(df.VIX.raw) <- gsub("Close.Last", "Close", colnames(df.VIX.raw))
  # same columns as vix: Date, Open, Low, High, Close
  df.VIX.raw <- df.VIX.raw %>% dplyr::select(Date, Open, Low, High, Close)
}
df.VIX <- df.VIX.raw

#####################################################################
### Add date information
stop()

df.VIX$Date <- ymd(df.VIX$Date)

### Make sure no missing dates
date.seq <- seq.Date(ymd(min(df.VIX$Date, na.rm=T)), ymd(max(df.VIX$Date, na.rm=T)), "days")
df.VIX <- merge(df.VIX, data.frame(Date=date.seq), by="Date", all.y=TRUE)

### Select range from January 1, 2010 (the start) to Nov 1 2020 (the present - don't do later)
df.VIX <- df.VIX %>% dplyr::filter((ymd(START.DS) <= Date)) %>% 
  dplyr::filter((Date <= ymd(END.DS)))

### Add information on non-trading days: weekends, holidays (to keep full dataset)
# VIX holidays are from:
# https://www.tradinghours.com/exchanges/VIX/market-holidays/2010
df.VIX$is_weekend <- wday(df.VIX$Date) %in% c(1,7) # end date (11/01/2020) is weekend
df.holidays <- data.frame(fread("VIX_trading_days__2010_2020.csv", sep = '\t')) %>%
  dplyr::filter(grepl("Closed", Status)) %>% dplyr::mutate(Date=as.Date(Observed.Date, format="%B %d, %Y"))
# sanity check: sum(is.na(df.holidays$Date)) == 0
df.VIX$is_holiday <- df.VIX$Date %in% df.holidays$Date

### final info on whether is a trading day or not
df.VIX$is_trading_day <- with(df.VIX, (!is_weekend) & (!is_holiday) )

#####################################################################
### Check date accuracy

# after all processing, % of observations that have trading days without data
# Ideal: for trading day = 0, should have 0; for trading day = 1, should have 100
# Use any/all columns Open, High, Low, Close to determine if data or not.
pprint("How many trading days have data?")
print(data.frame(df.VIX %>% group_by(is_trading_day) %>% dplyr::summarise(has_data=mean(!is.na(Close))))); pprint("")
num.cols <- c("Open", "High", "Low", "Close")

# Dates quality (1) it appears that there are a couple non-trading days (8/24/2004, 12/31/2010) that somehow have data.
# Fix: mark these as NA, should not have data. (if rerun, will yield 0 zeros)
pprint("Dates quality (1): non-trading days that have data, will set to NA.")
df.err.nontrading.data <- subset(df.VIX, (df.VIX$is_trading_day == 0) & (apply(df.VIX, 1, function(row) any(!is.na(row[num.cols])))))
print(df.err.nontrading.data)
df.VIX[rownames(df.err.nontrading.data), num.cols] <- NA
pprint(glue("{round(100 * nrow(df.err.nontrading.data) / nrow(df.VIX), 5)}% of dataset"))

# Dates quality (2) it appears that there are a few trading days without data
# Fix: will impute later. 
pprint("Dates quality (2): trading days without data, will impute.")
df.err.trading.data <- subset(df.VIX, (df.VIX$is_trading_day == 1) & (apply(df.VIX, 1, function(row) any(is.na(row[num.cols])))))
print(df.err.trading.data)
pprint(glue("{round(100 * nrow(df.err.trading.data) / nrow(df.VIX), 5)}% of dataset"))

# note the missing days coincide with vix.

#####################################################################
### Logging and summary statistics, missing rate, etc.
pprint("% NAs in each column (trading days only):")
print(sapply(df.VIX %>% dplyr::filter(is_trading_day == 1), function(x)
  data.frame(na_pct=mean(is.na(x), na.rm=T)*100.0, na_count=sum(is.na(x), na.rm=T), total_rows=length(x)) ))
pprint("summary stats (trading days only):")
print(sapply(df.VIX %>% dplyr::filter(is_trading_day == 1), function(x)
  data.frame(mean=mean(x, na.rm=T), var=var(x, na.rm=T)) )) # mak

#####################################################################
### Some imputation for the missing dates (non-trading days)
### remember, this is a volatility time series, NOT the underlying VIX.
### use forecast::na.interp()
### Reference: https://arxiv.org/pdf/1510.03924.pdf
df.VIX$Open[df.VIX$is_trading_day == 1] <- forecast::na.interp(df.VIX$Open[df.VIX$is_trading_day == 1])
df.VIX$Low[df.VIX$is_trading_day == 1] <- forecast::na.interp(df.VIX$Low[df.VIX$is_trading_day == 1])
df.VIX$High[df.VIX$is_trading_day == 1] <- forecast::na.interp(df.VIX$High[df.VIX$is_trading_day == 1])
df.VIX$Close[df.VIX$is_trading_day == 1] <- forecast::na.interp(df.VIX$Close[df.VIX$is_trading_day == 1])
pprint("% NAs in each column after imputation (trading days only):")
print(sapply(df.VIX %>% dplyr::filter(is_trading_day == 1), function(x)
  data.frame(na_pct=mean(is.na(x), na.rm=T)*100.0, na_count=sum(is.na(x), na.rm=T), total_rows=length(x)) ))
pprint("summary stats after imputation (trading days only):")
print(sapply(df.VIX %>% dplyr::filter(is_trading_day == 1), function(x)
  data.frame(mean=mean(x, na.rm=T), var=var(x, na.rm=T)) )) # make sure summary stats are roughly same before/after
pprint("")

#####################################################################
### Save cleaned data
fp.VIX.cleaned <- glue("{OUTPUT.DIR}/GSPC_cleaned__{format(START.DS, YYYYMMDD)}_{format(END.DS, YYYYMMDD)}.csv")
fwrite(df.VIX, file = fp.VIX.cleaned)
pprint(glue("Wrote cleaned data to {fp.VIX.cleaned}."))

# I think VIX focuses on close price (as opposed to vixo, vixh, vixl for
# open, high, low) so that is what I will focus on here.
# https://wrds-web.wharton.upenn.edu/wrds/tools/variable.cfm?
# library_id=4&file_id=54578&_ga=2.26234749.1305262340.1606181392-1951599496.1602196777


